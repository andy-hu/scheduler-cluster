package com.example.schedulercluster.scheduler;

import net.javacrumbs.shedlock.core.LockProvider;
import org.apache.curator.framework.CuratorFramework;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author huguangsheng
 * @version V1.0
 * @date 2019/5/9 下午5:50
 */
@Configuration
public class ZookeeperCuratorLockProvider {

    @Bean
    public LockProvider lockProvider(CuratorFramework client) {
        return new ZookeeperCuratorLockProvider(client);
    }

}
