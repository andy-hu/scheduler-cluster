package com.example.schedulercluster.scheduler;

import net.javacrumbs.shedlock.core.SchedulerLock;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author huguangsheng
 * @version V1.0
 * @date 2019/5/9 下午3:27
 */
@Component
public class TestClusterSchedulerJob {

    @Scheduled(cron = "0/5 * * * * ?")
    @SchedulerLock(name = "scheduledTaskName", lockAtMostForString = "PT4S", lockAtLeastForString = "PT4S")
    private void print() {
        System.out.println("scheduler.....");
    }

}
